#include "analyzer.h"

#include <fstream>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <numeric>
#include <ranges>
#include <cmath>


CAnalyzer::CAnalyzer(std::vector<double>&& numbers)
	: mNumbers(numbers)
{
}

CAnalyzer CAnalyzer::Create(const std::string& fileName)
{
	std::vector<double> numbers;

	// otevre soubor
	std::ifstream ifs(fileName, std::ios::in | std::ios::binary);

	if (!ifs.is_open())
		throw std::invalid_argument{ "Nelze otevrit soubor " + fileName };

	// zjisti velikost a pole cisel zvetsi, aby se tam cisla vesla
	ifs.seekg(0, std::ios::end);
	numbers.resize(ifs.tellg() / sizeof(double));
	ifs.seekg(0, std::ios::beg);

	// precte obsah souboru
	ifs.read(reinterpret_cast<char*>(numbers.data()), numbers.size() * sizeof(double));

	// overi, ze se precetlo vsechno
	if (ifs.gcount() != numbers.size() * sizeof(double))
		throw std::runtime_error{ "Nelze precist cisla ze souboru " + fileName };

	return CAnalyzer(std::move(numbers));
}

void CAnalyzer::Calculate_Density()
{
    // density := fce četností
    std::for_each(mNumbers.begin(), mNumbers.end(), [this](const auto& a){ ++density[std::round(a)]; });
 
    // transformace hodnot na <0;1> -> fce hustoty
    std::for_each(density.begin(), density.end(), [this](auto& a){ a.second /= mNumbers.size(); });
 
    // dump
    printMap(density);
}

void CAnalyzer::Calculate_Distribution()
{
    // výpočet kumulativni fce
    std::accumulate(density.cbegin(), density.cend(), 0.,
        [this](const auto& a, const auto& b){
            return distr[b.first] = a + b.second;
        }
    );

    // dump
    printMap(distr);
}

void CAnalyzer::Analyze_Distribution(double& mean, double& sigma) const noexcept
{
    // E(X)
    mean = std::accumulate(density.cbegin(), density.cend(), 0.,
        [](const auto& a, const auto& b) {
            return a + b.first * b.second;
        }
    );

    // Var(X)
    sigma = std::accumulate(density.cbegin(), density.cend(), 0.,
        [mean](const auto& a, const auto& b) {
            return a + b.second * std::pow(b.first - mean, 2);
        }
    );
   
    // sigma = Var(x)^.5
    sigma = std::sqrt(sigma);
}

double CAnalyzer::Get_Percentile(unsigned char pct) const
{
    const auto percentile = std::lower_bound(distr.cbegin(), distr.cend(), pct / 100.,
        [](const auto& a, const auto& b){ return a.second < b; });

	return percentile->first;
}

size_t CAnalyzer::Get_Numbers_Above_Percentile(unsigned char pct) const
{
    // find first above percentile
    const auto percentice_upper_bound = std::upper_bound(distr.cbegin(), distr.cend(), pct / 100.,
        [](const auto& a, const auto& b){
            return a < b.second;
        }
    );

    // (1 - P(pct)) * |S| 
    return (1 - percentice_upper_bound->second) * mNumbers.size();
}

void CAnalyzer::printMap(const decltype(density)& which) const {
    std::for_each(which.begin(), which.end(),
        [this](const auto& a){
            std::cout << a.first << "|" << a.second << "\n";
        }
    );
}
